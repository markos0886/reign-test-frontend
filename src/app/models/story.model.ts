export class Story {
  // tslint:disable-next-line: variable-name
  _id: string;
  storyId: string;
  title?: string;
  storyTitle?: string;
  url?: string;
  storyUrl?: string;
  author?: string;
  createdAt?: Date;
  isDeleted?: boolean;
}
