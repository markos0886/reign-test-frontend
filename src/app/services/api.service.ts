import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Story } from '../models/story.model';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  getAll(page: number) {
    return this.http.post<Array<Story>>(`${environment.serverUrl}api/all`, { page, limit: environment.pagination }).toPromise();
  }

  delete(id: string) {
    return this.http.delete<any>(`${environment.serverUrl}api/delete?id=${id}`).toPromise();
  }
}
