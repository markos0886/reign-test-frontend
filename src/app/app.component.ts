import { Component, HostListener, OnInit } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { ApiService } from './services/api.service';
import { environment } from '../environments/environment';
import { Story } from './models/story.model';
import * as moment from 'moment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  title: 'reign-test-frontend';
  page = 0;
  data: Story[] = [];
  canLoad = true;
  isMax = false;
  isLoading: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor(private api: ApiService) {
    moment.updateLocale('en', {
      calendar: {
        lastDay: '[Yesterday]', // to add time ([at] hh:mm a)
        sameDay: 'hh:mm a',
        nextDay: '[Tomorrow]',
        lastWeek: '[Last] dddd', // to add time ([at] hh:mm a)
        nextWeek: '[Next] dddd',
        sameElse: 'LL'
      }
    });
  }

  ngOnInit(): void {
    this.nextPage();
    this.isLoading.subscribe(value => this.canLoad = !value);
  }

  delete(item: Story, index: number) {
    this.api.delete(item.storyId).then(deleted => {
      this.data.splice(index, 1);
    }).catch(err => console.log(err));
  }

  nextPage() {
    this.isLoading.next(true);
    this.api.getAll(this.page).then(result => {
      this.data = this.data.concat(result);
      this.page++;
      if (result && result.length !== environment.pagination) {
        this.isMax = true;
      }
      this.isLoading.next(false);
    }).catch(err => {
      console.log(err);
      this.isLoading.next(true);
    });
  }

  onMouseOver(item: Story) {
    const button: HTMLElement = document.getElementById(item.storyId);
    button.style.display = 'block';
  }

  onMouseLeave(item: Story) {
    const button: HTMLElement = document.getElementById(item.storyId);
    button.style.display = 'none';
  }

  @HostListener('scroll', ['$event'])
  onElementScroll(event) {
    const element: HTMLElement = event.target;
    const pos = element.scrollTop + element.offsetHeight;
    const max = element.scrollHeight;
    if (pos === max && !this.isMax) {
      this.nextPage();
    }
  }
}
